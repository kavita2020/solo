Project requirements
	
	php (7.2.8)
	MySql (5.7.31)
	
Open terminal

    - Go to directory in which you want to clone the project.
		
	- Run clone command to download project from git 
	
		git clone https://bitbucket.org/kavita2020/solo.git

environment setup
	
    - Make a copy of .env.example file and save it as .env file in project root directory. 
	- Update following variable values with configuration credentials in .env file.

	For Mail:- MAILER_DSN

	For Database:- DATABASE_URL

	- Note:- If the username, password or host contain any character considered special in a URI (such as +, @, $, #, /, :, *, !), you must encode them. use the urlencode($userinput) function to encode them. 

Install project dependencies

	- Enter the project directory

    - Run following command to install
    
		composer install

Database setup

	- For creating database

	    php bin/console doctrine:database:create

	- To create tables in database.

	    php bin/console doctrine:migrations:migrate

	    - Note:- Respond ' yes ' to WARNING! You are about to execute a database migration that could result in schema changes and data loss. Are you sure you wish to continue? (yes/no)

Run symfony server

	- In order to run project on web browser run following command and then hit http://127.0.0.1:8000

		symfony server:start