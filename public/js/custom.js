$('.no_result_img').hide();

/* trigger cover pic submit */
$(document).on('change','#cover_pic',function() {
	$('#sbmt_cover_pic').trigger('click');
});

$("form#cover_pic_form").submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var form_url = $(this).attr("action");
    uploadPic(formData, form_url, 'cover_pic_tag');

});

/* trigger profile pic submit */
$(document).on('change','#profile_pic',function() {
	$('#sbmt_profile_pic').trigger('click');
});

$("form#profile_pic_form").submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var form_url = $(this).attr("action");
    uploadPic(formData, form_url, 'profile_pic_tag');
});

/* save cover and profile pic */
function uploadPic(formData, form_url, pic_img_tag){

	$.ajax({
        url: form_url,
        type: 'POST',
        data: formData,
        success: function (data) {
            if (data.status == "success") {
            	$('#'+pic_img_tag).attr('src',data.image_path);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

/* find user */
function findUser(node){
    var text_val = node.val();
    if (text_val != "") {
	   $('#sbmt_find_user').trigger('click');
    }
}

$("form#find_user_form").submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var form_url = $(this).attr("action");

    $.ajax({
    	url : form_url,
    	type : 'POST',
    	data : formData,
    	success: function(data){
            $('.no_result_img').hide(); 
            $('.user_search_li').html('');
    		if (data.count > 0) {
                $('.user_search_li').html(data.html);
            }

            if (data.count == 0){
                $('.no_result_img').show(); 
            }
    	},
        cache: false,
        contentType: false,
        processData: false
    })
});

function sentFrndRequest(receiver_id, sender_id, url, this_node) {
    if (receiver_id != "" && sender_id != "") {
        $.ajax({
            url: url,
            type: 'POST',
            data: {'sender_id':sender_id, 'receiver_id':receiver_id},
            success: function(data){
                // console.log(data);
                if (data.status == "success"){
                    if (data.message == "Request sent successfully"){
                        var parent_div = $(this_node).parent('.resp_btn');
                        parent_div.html('');
                        parent_div.html(data.btn);
                    }
                }
            }
        });
    }
}

function acceptFrndRequest(req_id, url, this_node){
    if (req_id != ""){
        $.ajax({
            url: url,
            type: 'POST',
            data: {'req_id':req_id},
            success:function(data){
                if (data.status == "success"){
                    var parent_div = $(this_node).parent('.resp_btn');
                    parent_div.html('');
                    parent_div.html(data.btn);
                }
            }
        });            
    }
}

function cancelFrndRequest(req_id, url, this_node){
    if (req_id != ""){
        $.ajax({
            url: url,
            type: 'POST',
            data: {'req_id':req_id},
            success:function(data){
                if (data.status == "success"){
                    var parent_div = $(this_node).parent('.resp_btn');
                    parent_div.html('');
                    parent_div.html(data.btn);
                }
            }
        });            
    }   
}

/*************************************** POST SECTION ************************************************/
/* add post */
$("form#add_user_post").on("submit", function(event){
    event.preventDefault();
    var url = $(this).attr('action');

    var formValues= $(this).serialize();

    $.post(url, formValues, function(data){
        if (data.status == "success"){
            $("form#add_user_post")[0].reset();
            $("#postModal").modal('hide');
            $('.post_list_div').prepend(data.html);
        }
    });
});

$(document).on('click','.del_post',function(){
    var url = $(this).attr("hitUrl");
    var post_div = $(this).closest('.post_list');
            
    $.get(url, function(data){
        if (data.status == "success"){
            post_div.remove();
        }
    });
});

$(document).on('click','.edit_post',function(){
    var url = $(this).attr("hitUrl");
    $.get(url, function(data){
        if (data.status == "success"){
            $('#editPostModal').html('');
            $('#editPostModal').html(data.html);
            $('#editPostModal').modal('show');
        }
    });
});

$(document).on("submit","form#edit_user_post",function(event){
    event.preventDefault();
    var url = $(this).attr('action');

    var formValues= $(this).serialize();

    $.post(url, formValues, function(data){
        if (data.status == "success"){
            $('#post'+data.id).html('');
            $('#post'+data.id).html(data.text);
            // $("form#editPostModal")[0].reset();
            $("#editPostModal").modal('hide');
        }

    });
});

/*****************************************CHAT SECTION **************************************/

$(document).on("submit", "form#send_msg_form", function(event){
    event.preventDefault();
    var url = $(this).attr('action');
    var formValues = $(this).serialize();
    $.post(url, formValues, function(data){

        if (data.status == "success") {
            $('.no_msg').css('display','none');
            $('#send_msg_input').val('');
            $('.msg_list').append(data.html);
            scrollMsgDiv();
            // $("#msg_list_div").animate({ scrollTop: $('#msg_list_div').prop('scrollHeight')}, 1000);
        }

    });
})

scrollMsgDiv();
setInterval(function(){ 
    var current_page_url = window.location.href;
    var ifmatch = current_page_url.search('/user/message');
    if (ifmatch != -1 ){
        getNewMsg(); 
    }
}, 
10000);
// setTimeout(function(){ getNewMsg(); }, 3000);
function getNewMsg(){
    var other_user_id = $('#other_user_id').val();
    var logged_user_id = $('#logged_user_id').val();
    var last_msg_id = $('#last_msg_id').val();

    var url = $('#get_msg_url').val();

    $.ajax({
        url: url,
        type: 'POST',
        data: {logged_user:logged_user_id, other_user:other_user_id, last_msg_id:last_msg_id},
        success:function(data){
            if (data.status == "success") {
                $('.msg_list').append(data.html);
                scrollMsgDiv();
                // $("#msg_list_div").animate({ scrollTop: $('#msg_list_div').prop('scrollHeight')}, 2000);   
                $('#last_msg_id').val(data.last_msg_id);    
            }
        }
    })
}

function scrollMsgDiv(){
    var Height = $('#msg_list_div').prop('scrollHeight');   
    // $("#msg_list_div").animate({ scrollTop: $('#msg_list_div').height()}, 1000);
    $('#msg_list_div').animate({scrollTop: Height}, 1200);    
}