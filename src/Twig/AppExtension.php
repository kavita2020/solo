<?php 

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
	public function getFilters() { 
        return array( 
            new TwigFilter('base64_encode', array($this, 'base64_en')),
            new TwigFilter('base64_decode', array($this, 'base64_de')),
            new TwigFilter('price', [$this, 'formatPrice']),
        );
    }

    public function base64_en($input) {
       return base64_encode($input);
    }

    public function base64_de($input) {
       return base64_decode($input);
    }

    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }
}

?>