<?php

namespace App\Repository;

use App\Entity\Messages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\UserRepository;

/**
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{
    private $userRepository;
    public function __construct(ManagerRegistry $registry, UserRepository $userRepository)
    {
        parent::__construct($registry, Messages::class);
        $this->userRepository = $userRepository;
    }

    public function create($data)
    {
        $sender_id = base64_decode($data['sender_id']);
        $receiver_id = base64_decode($data['receiver_id']);
        $sender = $this->userRepository->find($sender_id);
        $receiver = $this->userRepository->find($receiver_id);
        $msg = new Messages();
        $msg->setSender($sender);
        $msg->setReceiver($receiver);
        $msg->setMessage(base64_encode($data['message']));
        $msg->setStatus('unread');
        $msg->setCreatedAt(new \DateTime());
        $msg->setUpdatedAt(new \DateTime());
        $this->_em->persist($msg);
        $this->_em->flush();

        return $msg;
    }

    public function myMsg($logged_user_id, $other_user_id)
    {
        if ($logged_user_id != "" && $other_user_id != "") {
            $qb = $this->createQueryBuilder('a');
            $qb->select('m')
                ->from('App\Entity\Messages','m')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('m.sender', ':logged_user_id'),
                        $qb->expr()->eq('m.receiver', ':other_user_id')
                    )
                )
                ->orWhere(
                    $qb->expr()->andX(
                        $qb->expr()->eq('m.sender', ':other_user_id'),
                        $qb->expr()->eq('m.receiver', ':logged_user_id')
                    )
                )
                ->setParameter('logged_user_id', $logged_user_id)
                ->setParameter('other_user_id', $other_user_id)
            ;
            
            $result = $qb->getQuery()->getResult();
            return $result;
        }
    }

    public function newMessages($data)
    {
        if ($data) {
            $logged_user_id = base64_decode($data['logged_user']);
            $other_user_id = base64_decode($data['other_user']);
            $offset_id = base64_decode($data['last_msg_id']);
            $qb = $this->createQueryBuilder('a');
            $qb->select('m')
                ->from('App\Entity\Messages','m')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('m.sender', ':logged_user_id'),
                        $qb->expr()->eq('m.receiver', ':other_user_id')
                    )
                )
                ->orWhere(
                    $qb->expr()->andX(
                        $qb->expr()->eq('m.sender', ':other_user_id'),
                        $qb->expr()->eq('m.receiver', ':logged_user_id')
                    )
                )
                ->setParameter('logged_user_id', $logged_user_id)
                ->setParameter('other_user_id', $other_user_id)
            ;
            $qb->setFirstResult($offset_id);
            $result = $qb->getQuery()->getResult();
            // dd($result);
            return $result;
        }   
    }
    // /**
    //  * @return Messages[] Returns an array of Messages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Messages
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
