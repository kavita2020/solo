<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    private $passwordEncoder;

    public function __construct(ManagerRegistry $registry, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($registry, User::class);
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword, string $new_pass = null): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $user->setDecodedPass($new_pass);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function updateCoverImage(int $user_id, string $filename)
    {   
        $user = Self::findOneBy(['id'=>$user_id]);
        empty($filename) ? true : $user->setCoverPic($filename);
        
        $this->_em->persist($user);
        $this->_em->flush();
        return $user;
    }

    public function updateProfilePic(int $user_id, string $filename)
    {
        $user = Self::findOneBy(['id'=>$user_id]);
        empty($filename) ? true : $user->setProfilePic($filename);

        $this->_em->persist($user);
        $this->_em->flush();
        return $user;
    }

    public function findUsers(string $user_id, $data)
    {
        $users = $this->createQueryBuilder('u')
            ->where('u.isVerified = 1')
            ->andWhere('u.username LIKE :name')
            ->orWhere('u.name LIKE :name')
            ->setParameter('name', '%'.$data['name'].'%')
            ->andWhere('u.id <> :user_id')
            ->setParameter('user_id',$user_id)
            ->getQuery()
            ->getResult()
        ;
        return $users;
    }

    public function update($data)
    {
        if ($data) {
            $user_id = base64_decode($data['user_id']);
            $user = Self::find($user_id);
            // $user = Self::findOneBy(['id'=>$user_id]);
            empty($data['name']) ? true : $user->setName($data['name']);
            empty($data['phone']) ? true : $user->setPhone($data['phone']);
            
            $this->_em->persist($user);
            $this->_em->flush();
            
            return $user;
        }
    }

    public function isPasswordMatch(UserInterface $user, $data)
    {
        $checkPass = $this->passwordEncoder->isPasswordValid($user, $data['old_password']);
        if ($checkPass === true){

            $newPass = $data['password']['second'];
            $new_encode_pass = $this->passwordEncoder->encodePassword($user, $newPass);
            if ($new_encode_pass){
                $this->upgradePassword($user, $new_encode_pass, $newPass);
                return true;
            }
        }else{
            return false;
        }
    }

    public function updateAbout($data)
    {
        if ($data) {
            $user_id = base64_decode($data['user_id']);
            $user = Self::find($user_id);
            empty($data['about']) ? true : $user->setAbout($data['about']);
            
            $this->_em->persist($user);
            $this->_em->flush();
            
            return $user;
        }
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
