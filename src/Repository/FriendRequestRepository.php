<?php

namespace App\Repository;

use App\Entity\FriendRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\UserRepository;

/**
 * @method FriendRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method FriendRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method FriendRequest[]    findAll()
 * @method FriendRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FriendRequestRepository extends ServiceEntityRepository
{
    private $UserRepository;
    public function __construct(ManagerRegistry $registry, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct($registry, FriendRequest::class);
    }

    public function create($data)
    {

        if ($data) {
            $sender_id = base64_decode($data['sender_id']);
            $receiver_id = base64_decode($data['receiver_id']);
            $sender = $this->userRepository->findOneBy(['id'=>$sender_id]);
            $receiver = $this->userRepository->findOneBy(['id'=>$receiver_id]);
            $newReq = new FriendRequest();
            $newReq->setSender($sender);
            $newReq->setReceiver($receiver);
            $newReq->setStatus('pending');
            $this->_em->persist($newReq);
            $this->_em->flush();

            return $newReq;
        }
    }

    public function checkIfAlready($my_id, $other_id)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('f')
            ->from('App\Entity\FriendRequest','f')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('f.sender', ':my_id'),
                    $qb->expr()->eq('f.receiver', ':other_id')
                )
            )
            ->orWhere(
                $qb->expr()->andX(
                    $qb->expr()->eq('f.sender', ':other_id'),
                    $qb->expr()->eq('f.receiver', ':my_id')
                )
            )
            ->setParameter('my_id', $my_id)
            ->setParameter('other_id', $other_id)
        ;
        
        $result = $qb->getQuery()->getOneOrNullResult();
        
        return $result;
    }

    public function checkIfAlreadySent($data)
    {
        $sender = base64_decode($data['sender_id']);
        $receiver = base64_decode($data['receiver_id']);

        $result = $this->createQueryBuilder('f')
            ->where('f.sender = :sender_id')
            ->setParameter('sender_id', $sender)
            ->andWhere('f.receiver = :receiver_id')
            ->setParameter('receiver_id', $receiver)
            ->getQuery()
            ->getResult()
        ;  
        return $result;
    }

    public function accept($data)
    {
        $req_id = base64_decode($data['req_id']);
        $req = Self::find($req_id);
        if ($req) {
            $req->setStatus('accepted');
            $this->_em->persist($req);
            $this->_em->flush();
            
            return $req;

        }else{
            return "false";
        }
    }

    public function delete($data)
    {
        $req_id = base64_decode($data['req_id']);
        if ($req_id){
            $req = Self::find($req_id);
            if ($req) {
                $this->_em->remove($req);
                $this->_em->flush();

                return "true";
            }else{
                return "record not found";
            }
        }else{
            return "something went wrong";
        }
    }

    public function allFriends($user_id)
    {
        /*$result = $this->createQueryBuilder('f')
            ->where('f.status = :accepted')
            ->setParameter('accepted', 'accepted')
            ->andWhere('f.sender = :user_id')
            ->orWhere('f.receiver = :user_id')
            ->setParameter('user_id', $user_id)
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;*/


        $qb = $this->createQueryBuilder('a');
        $qb->select('f')
            ->from('App\Entity\FriendRequest','f')
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('f.sender', ':my_id'),
                    $qb->expr()->eq('f.receiver', ':my_id')
                )
            )
            ->andwhere(
                $qb->expr()->andX(
                    $qb->expr()->eq('f.status', ':accepted')
                )
            )
            ->setParameter('my_id', $user_id)
            ->setParameter('accepted', 'accepted')
        ;
        
        $result = $qb->getQuery()->getResult();
        
        return $result;
    }

    public function allPendingRequest($user_id)
    {
        $result = $this->createQueryBuilder('f')
            ->where('f.status = :pending')
            ->setParameter('pending', 'pending')
            ->andWhere('f.receiver = :user_id')
            ->setParameter('user_id', $user_id)
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        return $result;
    }

    // /**
    //  * @return FriendRequest[] Returns an array of FriendRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FriendRequest
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
