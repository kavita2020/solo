<?php

namespace App\Repository;

use App\Entity\Posts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Posts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Posts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Posts[]    findAll()
 * @method Posts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Posts::class);
    }

    public function create($data)
    {
        $post = new Posts();
        $post->setUser($data['user']);
        $post->setText(trim($data['text']));
        $post->setPrivacyId($data['privacy_id']);
        $post->setCreatedAt(new \DateTime());
        $post->setUpdatedAt(new \DateTime());
        $this->_em->persist($post);
        $this->_em->flush();
        
        return $post;
    }

    public function myposts($user_id)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->orderBy('p.id','DESC')
            ->getQuery()
            ->getResult()
        ;   
    }

    public function getUserPosts($user_id, $wefrnd = null)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('p')
            ->from('App\Entity\Posts','p')
            ->where('p.user = :user_id')
            ->setParameter('user_id', $user_id);
        
        if ($wefrnd == "yes"){

            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('p.privacy_id' , '2'),
                    $qb->expr()->eq('p.privacy_id', '3')
                )
            );

        }else{
            $qb->andWhere('p.privacy_id = 3');
        }
        
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getPublicPost($user_id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.privacy_id = 3')
            ->orWhere('p.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->orderBy('p.id','DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getPublicPrivatePost($user_id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.privacy_id = 3')
            ->orWhere('p.privacy_id = 2')
            ->orWhere('p.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->orderBy('p.id','DESC')
            ->getQuery()
            ->getResult()
        ;   
    }
    public function update($data)
    {
        $post = Self::find(['id'=>$data['post_id']]);
        
        if ($post) {
            $post->setText(trim($data['text']));
            $post->setPrivacyId($data['privacy_id']);
            $post->setCreatedAt(new \DateTime());
            $post->setUpdatedAt(new \DateTime());
            $this->_em->persist($post);
            $this->_em->flush();
            
            return $post;   
        }else{
            return "false";
        }
    }

    public function deletePost($post_id)
    {
        $post = Self::find(['id'=>$post_id]);
        if ($post) {
            $this->_em->remove($post);
            $this->_em->flush();
            return "true";
        }else{
            return "false";
        }
    }

    // /**
    //  * @return Posts[] Returns an array of Posts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Posts
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
