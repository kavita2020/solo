<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\PostsRepository;
use App\Repository\FriendRequestRepository;

class PostController extends AbstractController
{
	private $postsRepository;
	private $security;
    private $friendRequestRepository;

	public function __construct(PostsRepository $postsRepository,  Security $security, UrlGeneratorInterface $urlGenerator, FriendRequestRepository $friendRequestRepository)
	{
		$this->postsRepository = $postsRepository;
		$this->security = $security;
		$this->urlGenerator = $urlGenerator;
        $this->friendRequestRepository = $friendRequestRepository;
	}

    public function index()
    {
    	$user = $this->security->getUser();
        $logged_user_id = $user->getId();
    	$posts = $this->postsRepository->getPublicPrivatePost($logged_user_id);
        $allPost = [];
        // dd($posts);
        if (count($posts) > 0){
            foreach ($posts as $post) {
                $post_user_id = $post->getUser()->getId();

                if ($post_user_id == $logged_user_id){
                
                    array_push($allPost, $post); 
                
                }else{

                    $post_privacy = $post->getPrivacyId();
                    if ($post_privacy == 3){
                        array_push($allPost, $post);     
                    }

                    if ($post_privacy == 2){

                        $ifFrnd = $this->friendRequestRepository->checkIfAlready($logged_user_id, $post_user_id);
                        
                        if ($ifFrnd) {
                            $status = $ifFrnd->getStatus();
                            if ($status == "accepted") {
                                array_push($allPost, $post); 
                            }
                        }
                    }
                }
            }
        }
        return $this->render('post/index.html.twig', [
        	'posts' => $allPost
        ]);
    }

    public function addPost(Request $request) : JsonResponse
    {
    	$data = $request->request->all();
    	$user = $this->security->getUser();
        $data['user'] = $user;
    	if ($data) {
    		$add = $this->postsRepository->create($data);
    		if ($add) {
    			$post_id = $add->getId();
    			$post_user_name = $add->getUser()->getName();
    			$result['status'] = "success";
    			$result['message'] = "Post added successfully";
    			$html = "";
				$username = $user->getUsername();
				$imagename = $user->getProfilePic();
				if (!$imagename){
				    $imagename = "fallback.jpg";
				}

				$created_arr = json_decode(json_encode($add->getCreatedAt()), true);
				$created_date = $created_arr['date'];
				$created_at = date('M d, Y h:i A', strtotime($created_date));
				
				$updated_arr = json_decode(json_encode($add->getUpdatedAt()), true);
				$updated_date = $updated_arr['date'];
				$updated_at = date('M d, Y h:i A', strtotime($updated_date));
				
				$post_text = $add->getText();

                $encode_post_id = base64_encode($post_id);
				$editUrl = $this->urlGenerator->generate('post_detail', ['id'=>$encode_post_id,'type'=>'model']);
        		$deleteUrl = $this->urlGenerator->generate('post_delete', ['id'=>$encode_post_id]);
 				$edit_options = "";
 				if ($user->getId() == $add->getUser()->getId()) {
 					$edit_options = '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    	<i class="fas fa-ellipsis-v"> </i>
			  	</button>
			  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
				  	<li class="dropdown-item dropdown_link edit_post" hitUrl="'.$editUrl.'"> Edit </li>
			  		<li class="dropdown-item dropdown_link del_post" hitUrl="'.$deleteUrl.'">Delete</li>
				</ul>';
                $user_profile_url = $this->urlGenerator->generate('home', ['id'=>base64_encode($add->getUser()->getId())]);
 				}
$html .=<<<EOD
	<div class="post_list">
    	<div class="post_header">
    		<div class="dropdown right_side">
			  	$edit_options
			</div>
    		<img src="/images/profile_pic/$imagename" class="header_pro_pic">
        	
	        <span class="post_author">  <a href="$user_profile_url" class="post_author"> $post_user_name </a> </span>
	        <span class="post_time">
	        	<i class="fas fa-clock"> </i> $created_at
        	</span>
    	</div>
    	<span class="post_detail" id="post$post_id">
			$post_text
		</span>
    </div>
EOD;
			$result['html'] = $html;
    		}
    	}else{
    		$result['status'] = "fail";
    		$result['message'] = "Enter data";
    	}
    	return new JsonResponse($result);
    }

    public function postDetail($id, $type = null)
    {
        $id = base64_decode($id);
    	$post = $this->postsRepository->find(['id'=>$id]);
    	if ($post){
    		if($type == "model"){
    			$post_text = $post->getText();
    			$post_id = $post->getId();
        		$user_id = $post->getUser()->getId();
        		$post_privacy = $post->getPrivacyId();
    			$oneCheck = "";
    			$twoCheck = "";
    			$threeCheck = "";
    			$checkProp = "checked";
    			if ($post_privacy == 1){
    				$oneCheck = $checkProp;
    			}
    			if ($post_privacy == 2){
    				$twoCheck = $checkProp;
    			}
    			if ($post_privacy == 3){
    				$threeCheck = $checkProp;
    			}
				$updateUrl = $this->urlGenerator->generate('post_update');
    			$html = "";
$html .=<<<EOD
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Post</h4>
            </div>
            <form method="post" action="$updateUrl" id="edit_user_post"> 
                <input type="hidden" name="post_id" value="$post_id"> 
                <input type="hidden" name="user_id" value="$user_id"> 
                <div class="modal-body">
                    <div class="form-group">
                        <textarea class="form-control" name="text" rows="3" placeholder="Write post..."> $post_text </textarea>
                    </div>
                    <div class="form-group">
                        <label>Who can see this post?</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="privacy_id" id="privacy1" value="1" $oneCheck>
                          <label class="form-check-label" for="privacy1"> Only me </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="privacy_id" id="privacy2" value="2" $twoCheck>
                          <label class="form-check-label" for="privacy2"> Friends </label>
                        </div>
                        <div class="form-check disabled">
                          <input class="form-check-input" type="radio" name="privacy_id" id="privacy3" value="3" $threeCheck>
                          <label class="form-check-label" for="privacy3"> Public <label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-lg" value="Post">
                </div>
            </form>
        </div>
    </div>
EOD;
				$result['status'] = "success";
				$result['html'] = $html;
    		}else{
    			$result['status'] = "fail";
    			$result['message'] = "Post not found";
    		}
    	}else{

	    	$result['status'] = "fail";
	    	$result['message'] = "Something went wrong";
    	
    	}
    	return new JsonResponse($result);
    }

    public function updatePost(Request $request)
    {
    	$data = $request->request->all();
    	if ($data) {

	    	$update = $this->postsRepository->update($data);
	    	if ($update) {
	    		$result['status'] = "success";
	    		$result['text'] = trim($update->getText());
	    		$result['id'] = $update->getId();
	    	}else{
	    		$result['status'] = "fail";
	    		$result['message'] = "Something went wrong.";
	    	}
    	}else{
    		$result['status'] = "fail";
	    	$result['message'] = "Something went wrong.";
    	}

    	return new JsonResponse($result);
    }

    public function deletePost($id)
    {
        $id = base64_decode($id);
    	$del = $this->postsRepository->deletePost($id);

    	$result['status'] = "success";
    	$result['message'] = "Deleted";
    	return new JsonResponse($result);

    }
}