<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Symfony\Component\Security\Core\Security;
use App\Repository\PostsRepository;
use App\Repository\FriendRequestRepository;

class HomeController extends AbstractController
{
	private $fileUploader;
	private $UserRepository;
	private $security;
    private $postsRepository;
    private $friendRequestRepository;    


	public function __construct(FileUploader $fileUploader, UserRepository $UserRepository, Security $security,  PostsRepository $postsRepository, FriendRequestRepository $friendRequestRepository)
	{
		$this->fileUploader = $fileUploader;
		$this->UserRepository = $UserRepository;
		$this->security = $security;
        $this->postsRepository = $postsRepository;
        $this->friendRequestRepository = $friendRequestRepository;
	}

    public function index($id = null)
    {
        $logged = $this->security->getUser();
        if ($id) {
            $id = base64_decode($id);
            $user = $this->UserRepository->find($id);
            $isId = "yes";

            if ($logged->getId() == $id) {
                $isId = "no";
            }
        }else{
            $user = $logged;
            $isId = "no";
        }
        
        $user_id = $user->getId();
        $posts = [];
        
        if ($logged->getId() == $user->getId()){
            $posts = $this->postsRepository->myposts($user_id);
        }else{
            $wefrnd = "no";
            $ifFrnd = $this->friendRequestRepository->checkIfAlready($logged->getId(), $user_id);
            
            if ($ifFrnd) {
                $status = $ifFrnd->getStatus();
                if ($status == "accepted") {
                    $wefrnd = "yes";
                }
            }

            $posts = $this->postsRepository->getUserPosts($user_id, $wefrnd);
        }
        
        return $this->render('home/index.html.twig', [
            'posts' => $posts,
            'user' => $user,
            'isUrlHasId' => $isId
        ]);
    }

    /* update profile and cover photo */

    public function upload_pic(Request $request): JsonResponse
    {
    	$pic_type = $photo = $request->request->get('pic_value');
    	$photo = $request->files->get($pic_type);
    	$user = $this->security->getUser();
    	$user_id = $user->getId();

    	if ($pic_type == "cover_pic") {
    		$old_pic_name = $user->getCoverPic();    	
    	}

    	if ($pic_type == "profile_pic"){
    		$old_pic_name = $user->getProfilePic();
    	}

    	if ($photo) {

    		$return = $this->fileUploader->upload($photo, $pic_type);
    		
    		if ($return['status'] == "success"){
    		
    			$filename = $return['filename'];
    			if ($pic_type == "cover_pic") {
    				$update = $this->UserRepository->updateCoverImage($user_id, $filename);
	    			$saved_pic = $update->getCoverPic();
    			}

    			if ($pic_type == "profile_pic") {
	    			$update = $this->UserRepository->updateProfilePic($user_id, $filename);
	    			$saved_pic = $update->getProfilePic();
    			}

    			if($saved_pic == $filename){
    				if ($old_pic_name) {
    					$this->fileUploader->deleteOld($old_pic_name, $pic_type);
    				}
    			}
    			$return['image_path'] = "/images/".$pic_type."/".$filename;
    			$result = $return; 

    		}else{
    			$result['status'] = 'fail';
    			$result['error'] = "Something went wrong. Please try again";	
    		}
    	}else{
    		$result['status'] = 'fail';
    		$result['error'] = "Please select a image";
    	}

    	return new JsonResponse($result);
    }
}
