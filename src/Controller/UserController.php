<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\FriendRequestRepository;
use App\Repository\MessagesRepository;
use App\Entity\User;
use App\Form\AccountUpdateFormType;
use App\Form\ResetPasswordFormType;

class UserController extends AbstractController
{
    private $UserRepository;
    private $security;
    private $urlGenerator;
    private $friendRequestRepository;    
    private $messagesRepository;    

    public function __construct(UserRepository $UserRepository, Security $security, UrlGeneratorInterface $urlGenerator, FriendRequestRepository $friendRequestRepository, MessagesRepository $messagesRepository)
    {
        $this->UserRepository = $UserRepository;
        $this->security = $security;
        $this->urlGenerator = $urlGenerator;
        $this->friendRequestRepository = $friendRequestRepository;
        $this->messagesRepository = $messagesRepository;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    public function findUser(Request $request)
    {
		$data = $request->request->all();
        $user = $this->security->getUser();
        $my_id = $user->getId();
        $encode_my_id = base64_encode($my_id);
    	$list = $this->UserRepository->findUsers($my_id, $data);
        $total = count($list);
        
        $html = "";
        if ($total > 0) {
            foreach ($list as $users) {

                $this_user_id = $users->getId();
                $encode_this_user_id = base64_encode($users->getId());
                $ifalready = $this->friendRequestRepository->checkIfAlready($my_id, $users->getId());
                
                if ($ifalready) {
                    $req_id = $ifalready->getId();
                    $encode_req_id = base64_encode($req_id);
                    $req_status = $ifalready->getStatus();
                    if ($req_status == "accepted"){
                        
                        if ($my_id == $ifalready->getSender()->getId()){
                            $other_user_id = $ifalready->getReceiver()->getId();
                        }else{
                            $other_user_id = $ifalready->getSender()->getId();
                        }

                        $clickBtn = $this->messageBtn($other_user_id);
                    }else{
                        $sender_id = $ifalready->getSender()->getId();

                        if ($sender_id == $my_id){
                            $clickBtn = $this->cancelReqBtn($req_id);
                        }

                        $receiver_id = $ifalready->getReceiver()->getId();
                        
                        if ($receiver_id == $my_id){
                            $clickBtn = $this->acceptRejectReqBtn($req_id);
                        }
                    }
                }else{
                    $clickBtn = $this->sendReqBtn($this_user_id,$my_id);
                }
                
                $username = $users->getUsername();
                $imagename = $users->getProfilePic();
                if (!$imagename){
                    $imagename = "fallback.jpg";
                }
                $user_profile_url = $this->urlGenerator->generate('home', ['id'=>base64_encode($users->getId())]);
$html .=<<<EOD
    <li>
        <div class="result_li">
            <img src="/images/profile_pic/$imagename">
            <span class="search_name">
                <a href="$user_profile_url" class="post_author">
                $username  
                </a>
            </span>
            <div class="resp_btn">
                $clickBtn
            </div>
        </div>
    </li> 
EOD;
            }
        } 
        return new JsonResponse([
            // 'users'=>$list,
            'html' => $html, 
            'count' => $total
        ]);
    }

    public function sentFriendRequest(Request $request)
    {
        $data = $request->request->all();
        if ($data['sender_id'] != "" and $data['receiver_id'] != ""){
            $already_count = $this->friendRequestRepository->checkIfAlreadySent($data);
            if (count($already_count) == 0) {
                $add = $this->friendRequestRepository->create($data);
                $cancelBtn = $this->cancelReqBtn($add->getId());
                $result['status'] = "success";
                $result['message'] = "Request sent successfully";
                $result['btn'] = $cancelBtn;    
            }else{
                $result['status'] = "fail";
                $result['message'] = "Request already sent";
            }
        }else{
            $result['status'] = "fail";
            $result['message'] = "Something went wrong";
        }
        return new JsonResponse($result);
    }

    public function cancelFriendRequest(Request $request)
    {
        $data = $request->request->all();
        if ($data){
            $req = $this->friendRequestRepository->find(base64_decode($data['req_id']));
            $delete = $this->friendRequestRepository->delete($data);
            if ($delete == "true"){
                $receiver_id = $req->getReceiver()->getId();
                $sender_id = $req->getSender()->getId();
                $sendBtn = $this->sendReqBtn($receiver_id,$sender_id);

                $result['status'] = "success";
                $result['message'] = "Deleted successfully";
                $result['btn'] = $sendBtn;
            }else{                
                $result['status'] = "fail";
                $result['message'] = "status not changed";
            }
        }else{
            $result['status'] = "fail";
            $result['message'] = "Something went wrong";
        }

        return new JsonResponse($result);
    }

    public function acceptFriendRequest(Request $request)
    {
        $data = $request->request->all();
        if ($data){
            $accpt = $this->friendRequestRepository->accept($data);
            
            if ($accpt == false){
                $result['status'] = "fail";
                $result['message'] = "status not changed";
            }else{
                if ($accpt->getStatus() == "accepted"){
                    $logged_user_id = $this->security->getUser()->getId();

                    if ($logged_user_id == $accpt->getSender()->getId()){
                        $other_user_id = $accpt->getReceiver()->getId();
                    }else{
                        $other_user_id = $accpt->getSender()->getId();
                    }
                    $result['status'] = "success";
                    $result['message'] = "Request Accepted";
                    $result['btn'] = $this->messageBtn($other_user_id);
                }else{
                    $result['status'] = "fail";
                    $result['message'] = "status not changed";
                }
            }
        }else{
            $result['status'] = "fail";
            $result['message'] = "Something went wrong";
        }

        return new JsonResponse($result);
    }
    
    public function myFriends($id=null)
    {
        $requests = [];
        $logged = $this->security->getUser();
        $logged_user_id = $logged->getId();
        if ($id) {
            $id = base64_decode($id);
            $user = $this->UserRepository->find($id);
            $isId = "yes";

            if ($logged_user_id == $id) {
                $isId = "no";
            }
            
        }else{
            $user = $logged;
            $isId = "no";
            $requests = $this->friendRequestRepository->allPendingRequest($user->getId());
        }

        $friends = $this->friendRequestRepository->allFriends($user->getId());
        $matchFrnds = [];
        if (count($friends) > 0 ){
            foreach ($friends as $frend) {
                if ($user->getId() == $frend->getSender()->getId()){
                    $other_user = $frend->getReceiver()->getId();
                }else{
                    $other_user = $frend->getSender()->getId();
                }
                $matchfnd = [];
                $matchfnd['frend'] = $frend;
                
                $ifloggedfrnd = $this->friendRequestRepository->checkIfAlready($logged_user_id, $other_user);
                $clickBtn = "";
                if ($ifloggedfrnd) {
                    $req_status = $ifloggedfrnd->getStatus();
                    $req_id = $ifloggedfrnd->getId();
                    if ($req_status == "accepted"){
                        
                        if ($logged_user_id == $ifloggedfrnd->getSender()->getId()){
                            $other_user_id = $ifloggedfrnd->getReceiver()->getId();
                        }else{
                            $other_user_id = $ifloggedfrnd->getSender()->getId();
                        }

                        $clickBtn = $this->messageBtn($other_user_id);
                    }else{
                        $sender_id = $ifloggedfrnd->getSender()->getId();

                        if ($sender_id == $logged_user_id){
                            $clickBtn = $this->cancelReqBtn($req_id);
                        }

                        $receiver_id = $ifloggedfrnd->getReceiver()->getId();
                        
                        if ($receiver_id == $logged_user_id){
                            $clickBtn = $this->acceptRejectReqBtn($req_id);
                        }
                    }
                }else{
                    if ($logged_user_id != $other_user) {
                        $clickBtn = $this->sendReqBtn($other_user,$logged_user_id);
                    }
                }

                $matchfnd['btn'] = $clickBtn;
                array_push($matchFrnds,$matchfnd);
            }
        }
        return $this->render('user/my_friend.html.twig',[
            'user' => $user,
            // 'friends' => $friends,
            'friends' => $matchFrnds,
            'requests' => $requests,
            'isUrlHasId' => $isId
        ]);
    }

/**************************************** FRIENDS MESSAGES SECTION *************************************/

    public function messages(Request $request, $id)
    {   
        $user = $this->security->getUser();
        $logged_in_id = $user->getId();
        $other_user_id = base64_decode($id);
        $other_data = $this->UserRepository->find($other_user_id);
        // dd($user_id);
        $messages = $this->messagesRepository->myMsg($logged_in_id, $other_user_id);

        return $this->render('user/message.html.twig',[
            'other' => $other_data,
            'message' => $messages
        ]);
    }

    public function getNewMessage(Request $request)
    {
        $data = $request->request->all();
        $user = $this->security->getUser();
        $logged_in_id = $user->getId();
        $last_id = base64_decode($data['last_msg_id']);
        
        if ($data) {
            $newMsg = $this->messagesRepository->newMessages($data);
            if (count($newMsg) > 0) {
                $records = 0;
                $html = "";
                foreach ($newMsg as $msg) {
                    // dd($msg->getSender()); die;
                    if ($msg->getId() > $last_id){
                        $message = base64_decode($msg->getMessage());
                        $image = $msg->getSender()->getProfilePic();
                        if (!$image) {
                            $image = "fallback.jpg";
                        } 
                        $created_arr = json_decode(json_encode($msg->getCreatedAt()), true);
                        $created_date = $created_arr['date'];
                        $created_at = date('h:i A', strtotime($created_date));

                        $html = "";
                        if ($msg->getSender()->getId() == $logged_in_id){

// $html .=<<<EOD
//     <div class="bubbleWrapper">
//         <div class="inlineContainer own">
//             <img class="inlineIcon" src="/images/profile_pic/$image">
//             <div class="ownBubble own">
//              $message
//             </div>
//             <span class="own">$created_at</span>
//         </div>
//     </div>
// EOD;
                        }else{
$html .=<<<EOD
    <div class="bubbleWrapper">
        <div class="inlineContainer">
            <img class="inlineIcon" src="/images/profile_pic/$image">
            <div class="otherBubble other">
                $message
            </div>
            <span class="own">$created_at</span>
        </div>
    </div>
EOD;
                        }
                        $last_id = $msg->getId();
                        $records++;
                    }                
                }

                if ($records > 0) {
                    $result['status'] = "success";
                    $result['last_msg_id'] = base64_encode($last_id);
                    $result['html'] = $html;
                }else{
                    $result['status'] = "no new message";
                }
            }else{
                $result['status'] = "fail";
            }
        }else{
            $result['status'] = "success";
        }
        return new JsonResponse($result);
    }

    public function sendMessage(Request $request)
    {
        $data = $request->request->all();
        if ($data) {
            $add = $this->messagesRepository->create($data);
            if ($add) {
                $msg = base64_decode($add->getMessage());
                $image = $add->getSender()->getProfilePic();
                if (!$image) {
                    $image = "fallback.jpg";
                } 
                $html = "";

                $created_arr = json_decode(json_encode($add->getCreatedAt()), true);
                $created_date = $created_arr['date'];
                $created_at = date('h:i A', strtotime($created_date));

$html .=<<<EOD
    <div class="bubbleWrapper">
        <div class="inlineContainer own">
            <img class="inlineIcon" src="/images/profile_pic/$image">
            <div class="ownBubble own">
            $msg
            </div>
            <span class="own">$created_at</span>
        </div>
    </div>
EOD;

        $result['status'] = "success";
        $result['message'] = "Message sent successfully";
        $result['html'] = $html;          
            
            }
        }else{
            $result['status'] = "fail";
            $result['message'] = "Something went wrong";
        }

        return new JsonResponse($result);
    }

/******************************* USER ACCOUNT SECTION ************************************/

public function accountUpdate(Request $request)
{   
    $user = new User();
    /*$account = $this->createForm(AccountUpdateFormType::class);
    $account->handleRequest($request);*/
    $form = $this->createForm(ResetPasswordFormType::class);
    
    if ($request->isMethod('POST')) {

        $data = $request->request->all();
        
        if ($data) { 
            if ($data['form_type'] == "account") {
                $update = $this->UserRepository->update($data);
                $this->addFlash('success', 'Account details updated.');
            }elseif ($data['form_type'] == "password") {
            
                $data = $data['reset_password_form'];
                if ($data['password']['first'] != $data['password']['second']){
                    $this->addFlash('error', 'The password fields must match.');    
                }else{
                    $user = $this->getUser();
                    $change = $this->UserRepository->isPasswordMatch($user,$data);
                    if ($change == true) {
                        $this->addFlash('success', 'Password changed successfully.');
                    }else{
                        $this->addFlash('error', 'Old password does not matched');
                    }
                }

            
            }elseif ($data['form_type'] == "about") {
                $aboutUpdate = $this->UserRepository->updateAbout($data);
                $this->addFlash('success', 'About information updated.');
            }
            
            return $this->redirectToRoute("user_account_update");
        }
    }

    return $this->render('user/account_update.html.twig', [
        'passwordForm' => $form->createView(),
    ]);
}

public function accountInfo($id=null)
{
    $logged = $this->security->getUser();
    $logged_user_id = $logged->getId();
    if ($id) {
        $id = base64_decode($id);
        $user = $this->UserRepository->find($id);
        $isId = "yes";

        if ($logged_user_id == $id) {
            $isId = "no";
        }
        
    }else{
        $user = $logged;
        $isId = "no";
    }

    return $this->render('user/about_info.html.twig',[
        'user' => $user,
        'isUrlHasId' => $isId,
    ]);
}

/******************************* COMMON FUNCTIONS ************************************/

    public function sendReqBtn($reciever_id, $sender_id)
    {
        $encode_receiver_id = base64_encode($reciever_id);
        $encode_sender_id = base64_encode($sender_id);

        $hitUrl = $this->urlGenerator->generate('sent_friend_request');
        $clickBtn = '<input class="btn btn-primary" onclick="sentFrndRequest(\''.$encode_receiver_id.'\',\''.$encode_sender_id.'\',\''.$hitUrl.'\', $(this))" type="button" value="Send Request">';
                $html = "";
$html .=<<<EOD
    $clickBtn
EOD;
        
        return $html;
    }

    public function cancelReqBtn($req_id)
    {
        $encode_req_id = base64_encode($req_id);
        $hitUrl = $this->urlGenerator->generate('cancel_friend_request');
        $clickBtn = '<input class="btn btn-danger" onclick="cancelFrndRequest(\''.$encode_req_id.'\',\''.$hitUrl.'\',$(this))" type="button" value="Cancel Request">';
        $html = "";
$html .=<<<EOD
    $clickBtn
EOD;
        
        return $html;
    }

    public function acceptRejectReqBtn($req_id)
    {
        $encode_req_id = base64_encode($req_id);
        $acceptUrl = $this->urlGenerator->generate('accept_friend_request');
        $rejectUrl = $this->urlGenerator->generate('cancel_friend_request');
        $clickBtn = '<input class="btn btn-primary btn-divide" onclick="acceptFrndRequest(\''.$encode_req_id.'\',\''.$acceptUrl.'\',$(this))" type="button" value="Accept"> <input class="btn btn-danger btn-divide" onclick="cancelFrndRequest(\''.$encode_req_id.'\',\''.$rejectUrl.'\',$(this))" type="button" value="Delete">';   
        $html = "";
$html .=<<<EOD
    $clickBtn
EOD;
        
        return $html;    
    }

    public function messageBtn($other_user_id)
    {
        $other_user = base64_encode($other_user_id);
        $msgUrl = $this->urlGenerator->generate('message',['id'=>$other_user]);
        $clickBtn = '<a href="'.$msgUrl.'" class="btn btn-primary"> Message </a>';
        $html = "";
$html .=<<<EOD
    $clickBtn
EOD;
    
        return $html;
    }


}
