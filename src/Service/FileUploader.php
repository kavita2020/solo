<?php 

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Filesystem\Filesystem;

class FileUploader
{
    private $targetDirectory;
    private $slugger;

    public function __construct($targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    public function upload(UploadedFile $file, String $endpoint)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory().$endpoint, $fileName);
        	return ['status'=>'success','filename'=>$fileName];
        } catch (FileException $e) {
        	return ['status'=>'fail', 'error'=> $e->getMessage()];
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function deleteOld(String $file_name, String $endpoint)
    {
    	$fileSystem = new Filesystem();
    	$path = $this->getTargetDirectory().$endpoint.'/'.$file_name;
    	$fileSystem->remove($path);
    	return $fileSystem;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}

?>