<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;;
use Symfony\Component\HttpFoundation\Session\Session;

class CheckLoginListener
{
    protected $authorizationChecker;
    protected $router;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, UrlGeneratorInterface $router)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
    }

    public function onKernelRequest(RequestEvent  $event)
    {
        $route = $event->getRequest()->get('_route');
        if ($route == "index" || $route == "app_register" || $route == "app_login"){
            if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {

                $url = $this->router->generate('home');
                $response = new RedirectResponse($url);
                $event->setResponse($response);
            
            }
            $goNext = "yes";
        }else{
            $goNext = "no";
        }

        if ($goNext == "no"){
            if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
                
            }else{
                $session = new Session();
                
                $url = $this->router->generate('app_login');
                $session->getFlashBag()->add('error', 'You need to login first.');  
                $response = new RedirectResponse($url);
                $event->setResponse($response);
                
            } 
        }


    }
}

?>